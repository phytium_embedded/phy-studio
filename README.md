# PhyStudio

- 当前版本: [v0.1.3](./ChangeLog.md)

- 本项目是 PhyStudio 工具发布仓库，PhyStudio 是面向  Phytium 系列 CPU 的嵌入式软件开发工具包，整体分为两个部分： 
    - 1. 命令行开发环境，提供交叉编译链和使用 [Phytium Standalone SDK](https://gitee.com/phytium_embedded/phytium-standalone-sdk.git), [Phytium FreeRTOS SDK](https://gitee.com/phytium_embedded/phytium-free-rtos-sdk.git) 的最小环境
    - 2. 集成开发环境，提供交叉编译链和编辑、编译、调试和下载 Phytium Standalone SDK 工程的完整环境

## 集成开发环境

![](./ide/figs/project_wizard.png)
![](./ide/figs/disassembly.png)

- Windows 平台的集成开发环境基于 Eclipse CDT C/C++ 开发，提供了完整的 SDK 工程管理、代码编辑、编译构建功能，能够通过 J-Link/CMSIS DAP 等工具在开发板上完成镜像下载和调试，集成 TFTP 服务支持通过网络快速下载镜像到开发板
- 具体内容可以[参考](./ide/ide.md)
- 下载路径 [PhyStudio_Setup_V0.1.3.exe](https://pan.baidu.com/s/1azuFUjzg0jZgilQB4r1OlA)
> 提取码： phyt 

- 支持的平台包括
    - 硬件架构： AARCH64
    - 硬件平台： E2000D/Q Demo 板，飞腾派
    - SDK类型: Phytium Standalone SDK
    - 调试适配器： J-Link, CMSIS-DAP, DAP-Link

- 支持的特性包括
    - 导入 SDK 例程（支持部分例程）作为 PhyStudio 工程，以及新建 PhyStudio 工程
    - 编辑 C/C++/汇编代码，高亮提示和快速查找符号
    - 使用 CDT 配置器配置 PhyStudio 工程编译、链接选项
    - 使用 Sdkconfig 配置界面配置 PhyStudio 工程
    - 集成 TFTP 服务器，通过网口上传镜像到开发板启动
    - 集成 OpenOCD, GDB，通过 JTAG 接口下载和调试 Elf 镜像

## 命令行开发环境

### Windows 平台

![](./terminal/windows_x64/figs/msys2_env.png)

- Windows 平台的命令行开发环境基于 Msys2 开发，提供了交叉编译链、构建和调试工具，开发者可以获得类似 Linux 的开发体验，能够编译 SDK 工程，通过 Jlink/CMSIS DAP 等工具在开发板上完成镜像下载和调试
- 具体内容可以[参考](./terminal/windows_x64/terminal_msys64.md)
- 下载路径 [env_msys64.exe](https://pan.baidu.com/s/1UIiYRHWjx15fHjj8nJX0Zw)
> 提取码： PHYT

### Linux x64 平台 (Ubuntu)

![](./terminal/linux_x64/figs/ubuntu_env.png)

- Linux 命令行开发环境提供了交叉编译链、构建和调试工具，能够编译 SDK 工程，通过 J-Link/CMSIS DAP 等工具在开发板上完成镜像下载和调试
- 具体内容可以[参考](./terminal/linux_x64/terminal_ubuntu_x64.md)
- 下载 [env_ubuntu_x64.tar.gz](https://pan.baidu.com/s/1UIiYRHWjx15fHjj8nJX0Zw)
> 提取码： PHYT 

- 支持的平台包括
    - 硬件架构： AARCH64，AARCH32
    - 硬件平台： E2000D/Q Demo 板，飞腾派，D2000 Demo 板
    - SDK类型: Phytium Standalone SDK，Phytium FreeRTOS SDK
    - 调试适配器： J-Link, CMSIS-DAP, DAP-Link

- 支持的特性包括
    - 编译 SDK 自带的例程和新建 SDK makefile 工程
    - 集成 OpenOCD, GDB，通过 JTAG 接口下载和调试 Elf 镜像