# Linux 平台命令行开发环境 (Ubuntu x64)

- Ubuntu 平台的命令行开发环境集成了，
- 1. `setup_dev.py`, 用于更新开发环境路径到环境变量
- 2. `xpack-arm-none-eabi-gcc-12.2.1-1.2-linux-x64`，xpack 发布的[32位交叉编译链](https://github.com/xpack-dev-tools/arm-none-eabi-gcc-xpack)，用于 Windows 环境交叉编译 ARM32 目标码
- 3. `xpack-aarch64-none-elf-gcc-12.2.1-1.2-linux-x64`，xpack 发布的[64位交叉编译链](https://github.com/xpack-dev-tools/aarch64-none-elf-gcc-xpack)，用于 Windows 环境交叉编译 ARM64 目标码
- 4. `openocd`, OpenOCD 是一种开源的片上编程和调试软件(https://github.com/openocd-org/openocd)，本环境中的 openocd 基于开源软件修改后编译，如果需要修改后的源码，可以联系 `opensource_embedded@phytium.com.cn` 获取

## 获取和安装开发环境

- (1) 从下面的链接中下载开发环境安装包

- [env_ubuntu_x64.tar.gz](https://pan.baidu.com/s/1UIiYRHWjx15fHjj8nJX0Zw)
> 提取码： PHYT 


- (2) 下载完成后，在 Linux 控制台中输入下列命令进行解压
```
tar zxvf env_ubuntu_x64.tar.gz
```

- (3) 解压安装完成后，进入解压目录，运行下列命令更新开发环境路径到环境变量中

```
python3 ./setup_dev.py
```

- (4) 更新完成后，重启系统或者运行下列命令在当前控制台生效环境变量

```
source /etc/profile.d/phytium_dev.sh
```
> 注意上述命令更新环境变量只对当前控制台生效，之前打开的控制台不会自动生效，建议重启系统全局生效

- (5) 安装完成后，可以测试一下各组件是否能够使用

![](./figs/ubuntu_env.png)

## 下载 SDK 源码进行编译

- (6) 在 Linux 控制台，通过 git 拉取 Phytium Standalone SDK 的代码

```
git clone https://gitee.com/phytium_embedded/phytium-standalone-sdk.git ./phytium-standalone-sdk
```

- (7) 进入 SDK 源码路径下的工程开始编译

```
cd ./phytium-standalone-sdk/example/system/letter_shell_test/
make load_kconfig LOAD_CONFIG_NAME=e2000d_aarch64_demo_shell
make clean
make -j
```

![](./figs/ubuntu_cc.png)

## 通过 OpenOCD 和 GDB 进行调试

- (8) 编译生成镜像后，可以使用开发环境中的 OpenOCD 和 GDB 进行调试，调试方法可以[参考](../jtag_debugging/jtag_debugging.md)