# Windows 平台命令行开发环境 (Msys2)

- Windows 平台的命令行开发环境基于 Msys2 开发，集成了，
- 1. `msys2` 命令行环境，主要提供 Windows 上的 shell 命令行开发环境，[Msys2](https://repo.msys2.org/distrib/x86_64/) 包括了Cygwin （POSIX 兼容性层）和 MinGW-w64（从"MinGW-生成"）
- 2. `run_msys2.cmd`，用于打开和配置msys2 shell的脚本
- 3. `xpack-arm-none-eabi-gcc-12.2.1-1.2-win32-x64`，xpack 发布的[32位交叉编译链](https://github.com/xpack-dev-tools/arm-none-eabi-gcc-xpack)，用于 Windows 环境交叉编译 ARM32 目标码
- 4. `xpack-aarch64-none-elf-gcc-12.2.1-1.2-win32-x64` ，xpack 发布的[64位交叉编译链](https://github.com/xpack-dev-tools/aarch64-none-elf-gcc-xpack)，用于 Windows 环境交叉编译 ARM64 目标码
- 5. `openocd`, OpenOCD 是一种开源的片上编程和调试软件(https://github.com/openocd-org/openocd)，本环境中的 openocd.exe 基于开源软件修改后编译，如果需要修改后的源码，可以联系 `opensource_embedded@phytium.com.cn` 获取

## 获取和安装开发环境

- (1) 从下面的链接中下载开发环境安装包

- [env_msys64.exe](https://pan.baidu.com/s/1UIiYRHWjx15fHjj8nJX0Zw)
> 提取码： PHYT

- (2) 下载完成后，在 Windows 环境下双击自解压安装到指定目录，注意安装路径使用全英文和数字，不要使用带中文字符的路径
- (3) 解压安装完成后，双击脚本`run_msys2.cmd`, 进入 msys2 控制台，之后可以直接使用，使用前可以测试相关组件是否可以使用

![](./figs/msys2_env.png)

## 下载 SDK 源码进行编译

- (4) 在 msys2 控制台，通过 git 拉取 Phytium Standalone SDK 的代码

```
cd ~
git clone https://gitee.com/phytium_embedded/phytium-standalone-sdk.git ./phytium-standalone-sdk
```

- (5) 进入 SDK 源码路径下的工程开始编译

```
cd ./phytium-standalone-sdk/example/system/letter_shell_test/
make load_kconfig LOAD_CONFIG_NAME=e2000d_aarch64_demo_shell
make clean
make -j
```

![](./figs/sdk_cc.png)

## 通过 OpenOCD 和 GDB 进行调试

- (6) 编译生成镜像后，可以使用开发环境中的 OpenOCD 和 GDB 进行调试，调试方法可以[参考](../jtag_debugging/jtag_debugging.md)