# JTAG 调试器

- 本文将介绍如何使用 Phytium 系列 CPU 的 OpenOCD 调试环境，以及如何使用 GDB 来调试 Phytium Standalone 应用程序

## 工作原理

> 介绍 FJTAG 接口、OpenOCD 和 GDB 如何相互连接，从而实现 Phytium 系列 CPU 的调试功能

参考[sdk例程](https://gitee.com/phytium_embedded/phytium-standalone-sdk/tree/master/example/system/jtag_debugging)中README.md的例程介绍

## 选择 Phytium 目标板

### E2000D/Q Demo 板

介绍如何在 E2000D/Q Demo 板上连接 JTAG 适配器和调试上位机

参考[sdk例程](https://gitee.com/phytium_embedded/phytium-standalone-sdk/tree/master/example/system/jtag_debugging)中的硬件配置方法。

### 调试范例

> 通过调试一个 Standalone 例程，演示调试的方法

参考[sdk例程](https://gitee.com/phytium_embedded/phytium-standalone-sdk/tree/master/example/system/jtag_debugging)

### PhyStudio 调试 Standalone 程序

> 介绍如何使用 PhyStudio 工程进行调试

- [参考](../ide/ide.md) 中的`调试功能`相关介绍

### 命令行终端调试 Standalone 程序

> 介绍如何在命令行终端调试一个 Standalone 例程

* 目前支持Windows（Msys终端）和linux平台（Ubuntu20.04和Centos7），下述测试在VMVare+Centos7环境下完成。

* Ubuntu下安装依赖

```csharp
sudo apt-get install build-essential pkg-config autoconf automake libtool libusb-dev libusb-1.0-0-dev libsysfs-dev
```

```csharp
sudo apt-get install libhidapi-dev libjaylink-dev
```

hidapi对应DAP适配器， jaylink对应JLink适配器。

* Centos下安装依赖

```csharp
yum install libtool libsystf autoconf automake 
libusblibusb-1.x libhidapi libjaylink(需通过源码编译安装)
```

* 各依赖的版本要求:

  *autoconf >= 2.69*\
  *automake >= 1.14*\
  *gcc >= 8.0*\
  *GLIBC库 >= 2.34*

#### 1\. 编译程序镜像

make menuconfig 设置以下编译选项：

* 关闭 CONFIG_DEBUG_NOOPT，不进行编译优化

![Alt text](pic/image36.png)

* 使能 CONFIG_DEBUG_SYMBOLS，将调试符号完整地编译进入 ELF 镜像

![Alt text](pic/image37.png)

* 使能 CONFIG_OUTPUT_ASM_DIS，CONFIG_DEBUG_LINK_MAP，输出反汇编文件,方便调试时查阅符号

![Alt text](pic/image38.png)

#### 2\. 启动 OpenOCD 调试代理

> 介绍 OpenOCD 的启动方法和基础命令
> 介绍通过 telnet 登录 OpenOCD 后台

* 启动开发板;

* 进入openocd安装目录

```shell
cd env_ubuntu_x64/openocd
```

* 命令行配置启动文件，启动openocd

```shell
./bin/openocd -f ./share/openocd/scripts/interface/jlink.cfg -f ./share/openocd/scripts/target/e2000d_jlink_v9.cfg
```

* 启动完成，openocd进程会监听3333端口

![Alt text](pic/image-1.png)

#### 3\. 下载镜像文件

* 另外开启一个终端，把编译sdk生成的镜像文件baremetal.elf移动到\*/openocd_0.12.0/image下。

![Alt text](pic/image-2.png)

#### 4\. 启动 GDB 调试器

> 介绍 GDB 的启动方法和基础命令
> 介绍 gdbinit 文件的使用

1）设置启动gdb的环境变量，添加的路径为aarch-none-elf-gdb所在的位置（不同用户存放的位置不一致）

```shell
export PATH=$PATH:/mnt/env_ubuntu_x64/xpack-aarch64-none-elf-gcc-12.2.1-1.2-linux-x64/bin
```

2）启动aarch64-none-elf-gdb （启动gdb的位置最好与baremetal.elf所在位置一致，否则在gdb中启动elf文件需要使用补全elf所在路径）

```shell
aarch64-none-elf-gdb
```

![Alt text](pic/image-3.png)

3）gdb连接openocd

* gdb中执行命令：

```c
set remotetimeout 100000
target extended-remote localhost:3333
```

![Alt text](pic/image-4.png)

* 此时另一终端中的openocd显示已接收连接。

![Alt text](pic/image-5.png)

#### 5\. 启动镜像文件

* 载入镜像文件

```csharp
monitor init
monitor gdb_breakpoint_override hardware
load baremetal.elf
```

![Alt text](pic/image-6.png)

* 导入符号表

```csharp
file baremetal.elf
```

![Alt text](pic/image-7.png)

#### 6\. 设置和清除断点

1）设置断点：break or b 命令；

* 设置断点在符号（特定函数入口）

> b func

![Alt text](pic/image-8.png)

* 设置断点在特定地址

> b \*addr

![Alt text](pic/image-9.png)

2）查看断点：info breakpoints; i breakpoints命令

![Alt text](pic/image-10.png)

3）删除断点：delete命令

> delete 断点序号

![Alt text](pic/image-11.png)

#### 7\. 暂停和恢复程序运行

* 通过break命令暂停程序，详见第6条。

* continue指令恢复程序运行 （注：在开发板上调试不支持使用run指令，因为调试程序已在开发板上运行）

![Alt text](pic/image-12.png)

* 通过串口工具输入指令（md）触发断点

* 

![Alt text](pic/image-14.png)

#### 8\. 单步执行代码

* 介绍 step 单步运行 c 代码

> step or s\
> next or n

![Alt text](pic/image-21.png)

两者区别：next单步到程序源代码的下一行，不进入函数；step单步到程序执行的下一行，进入函数。

#### 10\. 查看断点上下文的代码

> list or l 命令

* 首先设置断点，再使用list命令查看断点上下文代码。

![Alt text](pic/image-20.png)

#### 11\. 查看函数调用栈

> backtrace or bt

![Alt text](pic/image-15.png)

> frame 栈帧序号                     //切换栈帧

![Alt text](pic/image-16.png)

#### 12\. 查看并设置系统寄存器

1）查看通用寄存器

> info registers or i registers

![Alt text](pic/image-17.png)

2）查看全部寄存器

> info all-registers

3）查看某一该寄存器

> info registers 寄存器名称

![Alt text](pic/image-18.png)

4）设置（PC）寄存器的值

> set var $pc=xxxxxx

#### 13\. 查看并设置内存

> x /<n/f/u> addr   \[参数为可选项\]

|参数|含义|可选值|
|-|-|-|
|n|显示的内存长度||
|f|显示的格式（默认16进制）|x(16进制)、d(十进制)、u(十进制无符号整型)、o(八进制)、t(二进制)、c(字符格式)、f(浮点格式)   ...|
|u|当前地址往后请求的位宽大小|b(单字节)、h(双字节)、w(四字节)、g(八字节)|

示例：

![Alt text](pic/image-19.png)

#### 14\. 观察和设置程序变量

1）查看变量值

* 查看全局和静态变量

> info variables

![Alt text](pic/image-22.png)

* 显示当前栈中的局部变量

> info locals

![Alt text](pic/image-23.png)

* 打印某一具体变量的值

> print var or p var\
> display var      //（display没有缩写形式）

![Alt text](pic/image-25.png)

2）设置变量值

> print var=x

![Alt text](pic/image-26.png)

> set var=x or
> set var \_var=x

* 注：第一个var作为gdb参数使用，用在变量名称和gdb参数相冲突的场景

![Alt text](pic/image-27.png)

#### 15\. 设置条件断点

含义：条件满足时，产生断点。

> b breakpoint if \[condition\]

![Alt text](pic/image-28.png)

> condition breakpoint_num var==x

![Alt text](pic/image-29.png)

#### 16\. 设置监测断点

含义：监测的值发生变化时，产生断点。

> watch \*addr

![Alt text](pic/image-30.png)

> watch \[condition\]

![Alt text](pic/image-31.png)

> watch var

![Alt text](pic/image-32.png)

#### 17\. 汇编指令

* 查看当前执行及其后20条指令

> display /20i $pc

![Alt text](pic/image-34.png)

* 调出汇编指令窗口配合调试

> layout asm

* 注：快捷键\[ctrl+x+a\]可退出窗口

![Alt text](pic/image-33.png)

* 进入下一条汇编指令

> si or ni 指令

![Alt text](pic/image-35.png)

#### 18\. 结束调试会话

> monitor shutdown //在gdb中关闭openocd

> quit or q //退出gd