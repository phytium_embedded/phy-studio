# PhyStudio v0.1.3 ChangeLog

Change Log since v0.0.1

## ide

- add PhyStudio reference doc

- add PhyStudio download link

- add PhyStudio debug doc

## terminal

- add terminal env reference doc

- add jtag debugging reference doc

- add terminal env package download link